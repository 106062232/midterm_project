var UserRef = firebase.database().ref('User');
var chatRef = firebase.database().ref("Chat");
var selected = [];
var selectedEmail = [];
var user_major = "";
var user_email = '';
var user_uid = "";
var chatKey = "";
var talkee = "";
var total_message = [];
var main_state = 0;//0 is main_blk, 1 is user_blk
var home_state = 0;//0 is home_blk, 1 is chat_blk
var obj_state = 0;//0 is object_blk, 1 is search_blk
const maxHeight_img = 20;
const maxWidth_img = 30;

async function init() {
    var userBlk = document.getElementById('user_blk');
    var mainBlk = document.getElementById('main_blk');
    var homeBlk = document.getElementById('home_blk');
    var chatBlk = document.getElementById('chat_blk');
    var objectBlk = document.getElementById('object_blk');
    var searchBlk = document.getElementById('search_blk');

    var searched_user = document.getElementById('searched_user');
    var selected_user = document.getElementById('selected_user')
    var searchBtn = document.getElementById("search_btn");
    var searchID = document.getElementById("searchuser");
    var chatRoom = document.getElementById("chat_room");
    var message = document.getElementById("message");
    var sendBtn = document.getElementById("send_btn");
    var total_object = [];
    
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            document.getElementById('home').style.cursor = 'pointer';
            document.getElementById('userInfo').style.cursor = 'pointer';
            document.getElementById('search').style.cursor = 'pointer';
            document.getElementById('login').style.display = 'none';
            document.getElementById('logout').style.display = 'block';
            document.getElementById('unsignin').style.display = 'none';
            objectBlk.innerHTML = "<span id='initialObj' ><img src='img/search.png' alt=''style='height:20px;width:20px;align-self:center'>Please go to search user</span>";
            objectBlk.addEventListener('click', () => {
                if (objectBlk.firstChild.id == 'initialObj') {
                    obj_state = 1;
                    objectBlk.style.display = 'none';
                    searchBlk.style.display = 'flex';
                }
            })

            //登出Log out
            document.getElementById("logout").addEventListener('click', async () => {
                if (confirm('確定登出?') == true) {
                    await firebase.auth().signOut().then( () => {
                        document.getElementById('login').style.display='block';
                        document.getElementById('logout').style.display='none';
                        if (home_state == 1) {
                            homeBlk.style.display = 'block';
                            chatBlk.style.display = 'none';
                            home_state = 0;
                        }
                        if (main_state == 1) {
                            mainBlk.style.display = 'flex';
                            userBlk.style.display = 'none';
                            main_state = 0;
                        }
                        if (obj_state == 1) {
                            objectBlk.style.display = 'flex';
                            searchBlk.style.display = 'none';
                            obj_state = 0;
                        }
                        window.location.href='signin.html';
                    }).catch(error => {alert(error.messege);})
                }
            })

            

            //Initial userInfo
            firebase.database().ref('User/'+user.uid).once('value').then(snapshot => {
                document.getElementById('Email').innerHTML = snapshot.val().UserEmail;
                document.getElementById('Name').innerHTML = snapshot.val().UserName;
                document.getElementById('UID').innerHTML = user.uid;
                user_major = snapshot.val().UserName;
                user_email = snapshot.val().UserEmail;
                user_uid = snapshot.key;
                document.getElementById('top_blk').innerHTML = "<img src='img/InfoIcon"+snapshot.val().UserIcon+".png' alt='InfoIcon' class='InfoIcon' id='currentIcon'><div style='height:100%;margin:0 0 0 3%;flex-direction:column;align-items:center;'><span id='information' style='font-size:300%'>UserInfo</span><input id='changeIcon' type='button' value='ChangeIcon' onclick=appearIcon()></div>"
            }).catch(error => {alert(error.messege);})
            
            ///Change state
            window.addEventListener('resize', () => {
                if (window.matchMedia('(max-width:700px)').matches) {initial_small_screen();}
                else {initial_big_screen();}
            })
            document.getElementById('userInfo').addEventListener('click', () => {
                if (main_state == 0) {
                    main_state = 1;
                    mainBlk.style.display = 'none';
                    userBlk.style.display = 'flex';
                }
                else {
                    main_state = 0;
                    mainBlk.style.display = 'flex';
                    userBlk.style.display = 'none';
                }

                if (window.innerWidth >= 700) {
                    obj_state = 0;
                    objectBlk.style.display = 'flex';
                    searchBlk.style.display = 'none';
                }
                else {
                    obj_state = 0;
                    if (home_state == 1) {
                        chatBlk.style.display = 'block';
                        objectBlk.style.display = 'none';
                    }
                    else {
                        chatBlk.style.display = 'none';
                        objectBlk.style.display = 'flex';
                    }
                    searchBlk.style.display = 'none';
                }
            })
            document.getElementById('home').addEventListener('click', () => {
                main_state = 0;
                mainBlk.style.display = 'flex';
                userBlk.style.display = 'none';
                home_state = 0;
                if (window.innerWidth >= 700) {homeBlk.style.display = 'block';}
                else {homeBlk.style.display = 'none';}
                chatBlk.style.display = 'none';
                obj_state = 0;
                objectBlk.style.display = 'flex';
                searchBlk.style.display = 'none';
            })
            document.getElementById('search').addEventListener('click', () => {
                searchID.value = "";
                searched_user.innerHTML = "";
                if (main_state == 0) {
                    if (obj_state == 0) {
                        obj_state = 1;
                        if (window.innerWidth < 700) {
                            home_state = 0;
                            chatBlk.style.display = 'none';
                        }
                        objectBlk.style.display = 'none';
                        searchBlk.style.display = 'flex';
                    }
                    else {
                        obj_state = 0;
                        objectBlk.style.display = 'flex';
                        searchBlk.style.display = 'none';
                    }
                }
            })

            //更新Icon
            firebase.database().ref('User').on('child_changed', data => {
                ///更新ChatRoom的Icon
                if (data.val().UserEmail == user_email) {
                    var email = user_email.split('@')[0];
                    var icon = data.child('UserIcon').val();

                    var obj = chatRoom.querySelectorAll(".deliver."+email);
                    for (var i = 0 ; i < obj.length ; i++) {
                        var old_icon = obj[i].children[2].src.slice(-5,-4);;
                        obj[i].children[2].src = 'img/Icon'+icon+'.png';
                        var message = obj[i].children[1].innerHTML;
                        var time = obj[i].children[0].innerHTML;
                        if (message == '') {
                            message = obj[i].children[1].src;
                            var H = obj[i].children[1].style.height;
                            var W = obj[i].children[1].style.width;
                            var oldMessage = "<div class='deliver "+email+"'><span class='time'>"+time+"</span> <img class='picture' src='"+message+"' style='height:"+H+";width:"+W+";'><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon deliver'></div>";
                            var replaceMessage = "<div class='deliver "+email+"'><span class='time'>"+time+"</span> <img class='picture' src='"+message+"' style='height:"+H+";width:"+W+";'><img src='img/Icon"+icon+".png' alt='icon' class='Icon deliver'></div>";
                            total_message.splice(total_message.indexOf(oldMessage), 1, replaceMessage);
                        }
                        else {
                            var oldMessage = "<div class='deliver "+email+"'><span class='time'>"+time+"</span> <span class='Message text'>"+message+"</span><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon deliver'></div>";
                            var replaceMessage = "<div class='deliver "+email+"'><span class='time'>"+time+"</span> <span class='Message text'>"+message+"</span><img src='img/Icon"+icon+".png' alt='icon' class='Icon deliver'></div>";
                            total_message.splice(total_message.indexOf(oldMessage), 1, replaceMessage);
                        }
                    }
                }
                else {
                    firebase.database().ref('Chat/'+chatKey).once('value', snapshot => {
                        if (snapshot.val().Multi) {
                            var objects = snapshot.val().Owner.split(',');
                            for (var i = 0 ; i < objects.length ; i++) {
                                if (data.val().UserEmail == objects[i]) {
                                    var email = objects[i].split('@')[0];
                                    var name =  data.child('UserName').val();
                                    var icon = data.child('UserIcon').val();

                                    var obj = chatRoom.querySelectorAll(".receiver."+email);
                                    for (var j = 0 ; j < obj.length ; j++) {
                                        var old_icon = obj[j].children[0].src.slice(-5,-4);;
                                        obj[j].children[0].src = 'img/Icon'+icon+'.png';
                                        var message = obj[j].children[1].innerHTML;
                                        var time = obj[j].children[2].innerHTML
                                        if (message == '') {
                                            message = obj[j].children[1].src;
                                            var H = obj[j].children[1].style.height;
                                            var W = obj[j].children[1].style.width;
                                            var oldMessage = "<div class='receiver "+email+"'><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon receiver'><img class='picture' src='"+message+"' style='height:"+H+";width:"+W+";'><span class='time'>"+time+"</span></div>";
                                            var replaceMessage = "<div class='receiver "+email+"'><img src='img/Icon"+icon+".png' alt='icon' class='Icon receiver'><img class='picture' src='"+message+"' style='height:"+H+";width:"+W+";'><span class='time'>"+time+"</span></div>";
                                            total_message.splice(total_message.indexOf(oldMessage), 1, replaceMessage);
                                        }
                                        else {
                                            var oldMessage = "<div class='receiver "+email+"'><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon receiver'><span class='Message text'>"+message+"</span> <span class='time'>"+time+"</span></div>";
                                            var replaceMessage = "<div class='receiver "+email+"'><img src='img/Icon"+icon+".png' alt='icon' class='Icon receiver'><span class='Message text'>"+message+"</span> <span class='time'>"+time+"</span></div>";
                                            total_message.splice(total_message.indexOf(oldMessage), 1, replaceMessage);
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            firebase.database().ref('User/'+user_uid+'/chatObject').once('value', snapshot => {
                                var objects = snapshot.val().split(',');
                                for (var i = 0 ; i < objects.length ; i++) {
                                    if (data.val().UserEmail == objects[i]) {
                                        var email = objects[i].split('@')[0];
                                        var name =  data.child('UserName').val();
                                        var icon = data.child('UserIcon').val();
        
                                        var obj = chatRoom.querySelectorAll(".receiver."+email);
                                        for (var j = 0 ; j < obj.length ; j++) {
                                            var old_icon = obj[i].children[0].src.slice(-5,-4);;
                                            obj[j].children[0].src = 'img/Icon'+icon+'.png';
                                            var message = obj[j].children[1].innerHTML;
                                            var time = obj[j].children[2].innerHTML
                                            if (message == '') {
                                                message = obj[j].children[1].src;
                                                var H = obj[j].children[1].style.height;
                                                var W = obj[j].children[1].style.width;
                                                var oldMessage = "<div class='receiver "+email+"'><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon receiver'><img class='picture' src='"+message+"' style='height:"+H+";width:"+W+";'><span class='time'>"+time+"</span></div>";
                                                var replaceMessage = "<div class='receiver "+email+"'><img src='img/Icon"+icon+".png' alt='icon' class='Icon receiver'><img class='picture' src='"+message+"' style='height:"+H+";width:"+W+";'><span class='time'>"+time+"</span></div>";
                                                total_message.splice(total_message.indexOf(oldMessage), 1, replaceMessage);
                                            }
                                            else {
                                                var oldMessage = "<div class='receiver "+email+"'><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon receiver'><span class='Message text'>"+message+"</span> <span class='time'>"+time+"</span></div>";
                                                var replaceMessage = "<div class='receiver "+email+"'><img src='img/Icon"+icon+".png' alt='icon' class='Icon receiver'><span class='Message text'>"+message+"</span> <span class='time'>"+time+"</span></div>";
                                                total_message.splice(total_message.indexOf(oldMessage), 1, replaceMessage);
                                            }
                                        }
                                        
                                        //更新ObjectBlk的Icon
                                        var changedObject = document.querySelector("#"+user_email.split('@')[0]+'\\,'+data.child('UserEmail').val().split('@')[0])
                                        changedObject.firstChild.src = 'img/Icon'+icon+'.png';
                                        var oldObject = "<div class='obj "+name+" "+old_icon+"' id='"+user_email+','+objects[i]+"' onclick=GetMessage('single',this.id,this.className)><img src='img/Icon"+old_icon+".png' alt='icon' class='Icon object'><span class='obj_searched'>"+name+"</span></div>";
                                        var replaceObject = "<div class='obj "+name+" "+icon+"' id='"+user_email+','+objects[i]+"' onclick=GetMessage('single',this.id,this.className)><img src='img/Icon"+icon+".png' alt='icon' class='Icon object'><span class='obj_searched'>"+name+"</span></div>";
                                        total_object.splice(total_object.indexOf(oldObject), 1, replaceObject);
                                    }
                                }
                            })
                        }
                    })
                }
            })
        }
        else {
            chatRoom.innerHTML = "";
            objectBlk.innerHTML = "";
            document.getElementById('home').style.cursor = 'default';
            document.getElementById('userInfo').style.cursor = 'default';
            document.getElementById('search').style.cursor = 'default';
            document.getElementById('unsignin').style.display = 'block';
        }
    });

    ///搜尋用戶
    searchBtn.addEventListener('click', () => {
        if (searchID.value != "") {
            var search_value = htmlspecialchars(searchID.value); ////防止嵌入
            var Found = false;
            var total_user = [];
            //遍歷User的子節點
            UserRef.once('value').then(snapshot => {
                //尋找是否存在此用戶
                snapshot.forEach(childsnapshot => {
                    if (childsnapshot.child('UserName').val() == search_value || childsnapshot.child('UserEmail').val() == search_value) {
                        Found = true;
                        total_user.push("<div class='obj' id='"+childsnapshot.child('UserName').val()+","+childsnapshot.child('UserEmail').val()+","+childsnapshot.key+"' onclick=SelectUser(this.id,"+childsnapshot.child('UserIcon').val()+")><img src='img/Icon"+childsnapshot.child('UserIcon').val()+".png' alt='icon' class='Icon object'><span class='obj_searched' >" + childsnapshot.child('UserEmail').val() + "</span></div>");
                    }
                })
                if (Found) {
                    searched_user.innerHTML = total_user.join("");
                }
                else {alert("用戶未存在！");}
                searchID.value = ""; ///清空input
            }).catch(e => console.log(e.message));
        }
    })
    document.getElementById('searchuser').addEventListener('keydown', event => {
        //按下Enter
        if (event.which == 13) {
            if (searchID.value != "") {
                var search_value = htmlspecialchars(search.value); ////防止嵌入
                var Found = false;
                var total_user = [];
                //遍歷User的子節點
                UserRef.once('value').then(snapshot => {
                    //尋找是否存在此用戶
                    snapshot.forEach(childsnapshot => {
                        if (childsnapshot.child('UserName').val() == search_value || childsnapshot.child('UserEmail').val() == search_value) {
                            Found = true;
                            total_user.push("<div class='obj' id='"+childsnapshot.child('UserName').val()+","+childsnapshot.child('UserEmail').val()+","+childsnapshot.key+"' onclick=SelectUser(this.id,"+childsnapshot.child('UserIcon').val()+")><img src='img/Icon"+childsnapshot.child('UserIcon').val()+".png' alt='icon' class='Icon object'><span class='obj_searched' >" + childsnapshot.child('UserEmail').val() + "</span></div>");
                        }
                    })
                    if (Found) {
                        searched_user.innerHTML = total_user.join("");
                    }
                    else {alert("用戶未存在！");}
                    searchID.value = ""; ///清空input
                }).catch(e => console.log(e.message));
            }
        }
    })

    //送出圖片
    upload.addEventListener('change', event => {
        var type = event.target.files[0].name.split('.');
        //只允許.jpg/.jpeg/.png檔案傳送
        if (type[type.length-1] == 'png' || type[type.length-1] == 'PNG' || type[type.length-1] == 'jpg' || type[type.length-1] == 'JPG' || type[type.length-1] == 'jpeg' || type[type.length-1] == 'JPEG') {
            var fileReader = new FileReader();
            fileReader.readAsDataURL(event.target.files[0]);
            fileReader.onload = event => {
                upload.value = "";
                ///Creat Image for get height & width
                var image = new Image();
                image.src = event.target.result;
                image.onload = () => {
                    chatRef.once('value').then( () => {
                        if (chatKey != "") {
                            var time = getTime();
                            firebase.database().ref('Chat/'+chatKey+'/message').push().set({
                                "type" : 'picture',
                                "deliver" : user_email,
                                "message" : event.target.result,
                                "time" : time,
                                "height" : image.height,
                                "width" : image.width,
                            })
                        }
                    }).catch(e => console.log(e.message));
                }
            }
        }
        else {alert('只能傳送.jpg/.jpeg/.png檔案！')}
        
    })

    //送出訊息
    sendBtn.addEventListener('click', function () {
        if (message.value != "") {
            chatRef.once('value').then( () => {
                if (chatKey != "") {
                    var time = getTime();
                    firebase.database().ref('Chat/'+chatKey+'/message').push().set({
                        "type" : 'text',
                        "deliver" : user_email,
                        "message" : htmlspecialchars(message.value),  ////防止嵌入
                        "time" : time,
                        "height" : 0,
                        "width" : 0,
                    })
                }
                message.value = ""; ///清空input
            }).catch(e => console.log(e.message));
        }
    });
    document.getElementById('message').addEventListener('keydown', event => {
        if (event.which == 13) {
            if (message.value != "") {
                chatRef.once('value').then( () => {
                    if (chatKey != "") {
                        var time = getTime();
                        firebase.database().ref('Chat/'+chatKey+'/message').push().set({
                            "type" : 'text',
                            "deliver" : user_email,
                            "message" : htmlspecialchars(message.value),
                            "time" : time,
                            "height" : 0,
                            "width" : 0,
                        })
                    }
                    message.value = ""; ///清空input
                }).catch(e => console.log(e.message));
            }
        }
    })

    ///顯示聊天對象及群組
    UserRef.once('value')
    .then(async snapshot => {
        var i = 0;
        var j = 0;
        var chatObject = snapshot.child(user_uid).child('chatObject').val(); ///取得user所有的聊天對象chatObject
        var groupObject = snapshot.child(user_uid).child('chatGroup').val(); ///取得user所有的聊天群組groupObject
        if (chatObject != ',' && chatObject != null) { ///user的聊天對象default為','
            chatObject = chatObject.split(',');
            for (; i < chatObject.length ; i++) {
                var objectName = '';
                var objectIcon = 1;
                ///從User的子節點中取得聊天對象的Name以及Icon
                await UserRef.once('value').then(snapshot => {
                    snapshot.forEach(childsnapshot => {
                        if (childsnapshot.child('UserEmail').val() == chatObject[i]) {
                            objectName = childsnapshot.child('UserName').val();
                            objectIcon = childsnapshot.child('UserIcon').val();
                        }
                    })
                }).catch(e => console.log(e.message));
                total_object.push("<div class='obj "+objectName+" "+objectIcon+"' id='"+user_email.split('@')[0]+","+chatObject[i].split('@')[0]+"' onclick=GetMessage('single',this.id,this.className)><img src='img/Icon"+objectIcon+".png' alt='icon' class='Icon object'><span class='obj_searched'>" + objectName + "</span></div>");
            }
            objectBlk.innerHTML = total_object.join("");
        }
        if (groupObject != ',' && groupObject != null) {
            groupObject = groupObject.split(',');
            for (; j< groupObject.length ; j++) {
                var groupName = '';
                await chatRef.once('value').then(snapshot => {
                    snapshot.forEach(childSnapshot => {
                        if (childSnapshot.key == groupObject[j]) {groupName = childSnapshot.child('GroupName').val();}
                    })
                })
                total_object.push("<div class='obj "+groupName+"' id='"+groupObject[j]+"' onclick=GetMessage('multi',this.id,this.className)><span class='obj_searched'><b>" + groupName + "</b></span></div>");
            }
            objectBlk.innerHTML = total_object.join("");
        }

        ///動態更新
        firebase.database().ref('User/'+user_uid).on('child_changed',async data => { ///取得user新增的聊天對象chatObject
            if (data.key == 'chatObject') {
                chatObject = data.val().split(',');
                for (; i < chatObject.length ; i++) {
                    var objectName = '';
                    var objectIcon = 1;
                    ///取得聊天對象的Name以及Icon
                    await UserRef.once('value').then(snapshot => {
                        snapshot.forEach(childsnapshot => {
                            if (childsnapshot.child('UserEmail').val() == chatObject[i]) {
                                objectName = childsnapshot.child('UserName').val();
                                objectIcon = childsnapshot.child('UserIcon').val();
                            }
                        })
                    }).catch(e => console.log(e.message));
                    total_object.push("<div class='obj "+objectName+" "+objectIcon+"' id='"+user_email.split('@')[0]+","+chatObject[i].split('@')[0]+"' onclick=GetMessage('single',this.id,this.className)><img src='img/Icon"+objectIcon+".png' alt='icon' class='Icon object'><span class='obj_searched'>" + objectName + "</span></div>");
                }
                objectBlk.innerHTML = total_object.join("");
            }
            else if (data.key == 'chatGroup') {
                groupObject = data.val().split(',');
                for (; j< groupObject.length ; j++) {
                    var groupName = '';
                    await chatRef.once('value').then(snapshot => {
                        snapshot.forEach(childSnapshot => {
                            if (childSnapshot.key == groupObject[j]) {groupName = childSnapshot.child('GroupName').val();}
                        })
                    })
                    total_object.push("<div class='obj "+groupName+"' id='"+groupObject[j]+"' onclick=GetMessage('multi',this.id,this.className)><span class='obj_searched'><b>" + groupName + "</b></span></div>");
                }
                objectBlk.innerHTML = total_object.join("");
            }
        })
    }).catch(e => console.log(e.message));

    ///通知Notificatioin
    if (window.Notification) {
        ///取得新增的message的聊天室owner
        chatRef.on('child_changed', snapshot => {
            var owners = snapshot.val().Owner.split(',');
            var multi = snapshot.val().Multi;
            var groupname = snapshot.val().GroupName;
            if (owners[0] == user_email || owners[1] == user_email) {
                ///搜尋Message節點下最尾端(limitToLast(1))的子節點
                firebase.database().ref('Chat/'+snapshot.key+'/message').orderByKey().limitToLast(1).once('value').then(snapshot => {
                    snapshot.forEach(childsnapshot => {
                        if (childsnapshot.val().deliver != user_major) {
                            if (Notification.permission == 'granted') {notify(childsnapshot.val(), multi, groupname);}
                            else {Notification.requestPermission().then(function(permission) {notify(childsnapshot.val(), snapshot.val().Multi, snapshot.val().GroupName);})}
                        }
                    })
                })
            }
        })
    }
    else {alert('此瀏覽器不支持Notification')} 
};

function SelectUser (user, icon) {
    var selected_name = user.split(',')[0];
    var selected_email = user.split(',')[1];
    var selected_uid = user.split(',')[2];
    var children = document.querySelector('#selected_user').children;
    var AlreadySelected = false;

    for (var i = 0 ; i < children.length ; i++) {
        if (children[i].id == user) {AlreadySelected = true;break;}
    }

    if (!AlreadySelected && selected_email != user_email) {
        selected.push(user);
        selectedEmail.push(selected_email);
        document.getElementById('selected_user').innerHTML += "<div class='obj' id='"+user+"' onclick='popSelected(this.id)'><img src='img/Icon"+icon+".png' alt='icon' class='Icon object'><span class='obj_searched' >"+selected_email+"</span></div>";
        document.getElementById('selected_user').style.display = 'flex'; //顯示selected
    }
    else {
        alert('錯誤的選擇')
        if (selected.length==0) {document.getElementById('selected_user').style.display = 'none';}
    }
    document.getElementById('searchuser').value = ""; ///清空input
    document.getElementById('searched_user').innerHTML = ""; ///清空div
}

function popSelected (user) {
    selected.splice(selected.indexOf(user), 1);
    selectedEmail.splice(selected.indexOf(user.split(',')[1]), 1);
    document.getElementById('selected_user').removeChild(document.getElementById(user));
    if (selected.length == 0) {
        document.getElementById('selected_user').style.display = 'none';
    }
}

///建立聊天
function BuildChat () {
    var objectNum = selected.length;
        var AlreadyStartChat = false;
        //遍歷Chat的子節點尋找該聊天室是否已建立
        chatRef.once('value').then(async snapshot => {
            snapshot.forEach(childsnapshot => {
                var owners = childsnapshot.child('Owner').val().split(',');
                if (childsnapshot.child('Multi').val() == true && objectNum > 1) {
                    var Multiusers = 0;
                    for (var i = 0 ; i < owners.length ; i++) {
                        for (var j = 0 ; j < objectNum ; j++) {
                            if (owners[i] == selected[j].split(',')[1] || owners[i] == user_email) {Multiusers++;break;}
                        }
                        if (Multiusers == objectNum + 1) {AlreadyStartChat=true;break;}
                    }
                }
                else if (childsnapshot.child('Multi').val() == false && objectNum == 1) {
                    var Singleusers = 0;
                    for (var i = 0 ; i < owners.length ; i++) {
                        if (owners[i] == selected[0].split(',')[1]) {Singleusers++;}
                        if (owners[i] == user_email) {Singleusers++;}
                    }
                    if (Singleusers == 2) {AlreadyStartChat=true;}
                }
            })
            
            if (!AlreadyStartChat) {
                if (objectNum != 1) { ///建立多人聊天室
                    var groupName = prompt('請輸入群組名稱',null);
                    if (groupName != null && groupName !=  '') {
                        //在database裡面建立一個新的Chat節點
                        selectedEmail.push(user_email);
                        var groupKey = chatRef.push().getKey();
                        firebase.database().ref('Chat/'+groupKey).set({
                            "Owner" : selectedEmail.join(','),
                            "Start Time" : getTime(),
                            "Multi" : true,
                            "GroupName" : groupName,
                        })
                        //更新雙方聊天對象groupObject
                        for (var i = 0 ; i < objectNum ; i++) {
                            var object = firebase.database().ref('User/'+selected[i].split(',')[2]);
                            await object.once('value').then(snapshot => {
                                var chatGroup = snapshot.child('chatGroup').val();
                                if (chatGroup == ',' || chatGroup == null) {object.update({"chatGroup" : groupKey});}
                                else {
                                    var wasUpdate = false;
                                    
                                    chatGroup = chatGroup.split(',');
                                    for (var i = 0 ; i < chatGroup.length ; i++) {
                                        if (chatGroup[i] == groupKey) {wasUpdate = true;break;}
                                    }
                                    if (!wasUpdate) {object.update({"chatGroup" : chatGroup +','+ groupKey});}
                                }
                            }).catch(e => console.log(e.message));
                        }

                        var subject = firebase.database().ref('User/'+user_uid)
                        subject.once('value').then(snapshot => {
                            var chatGroup = snapshot.child('chatGroup').val();
                            if (chatGroup == ',') {subject.update({"chatGroup" : groupKey});}
                            else {
                                var wasUpdate = false;
                                chatGroup = chatGroup.split(',');
                                for (var i = 0 ; i < chatGroup.length ; i++) {
                                    if (chatGroup[i] == search_email) {wasUpdate = true;break;}
                                }
                                if (!wasUpdate) {subject.update({"chatGroup" : chatGroup +','+ groupKey});}
                            }
                        }).catch(e => console.log(e.message));
                    }
                }
                else { ///建立一對一聊天室
                    if (window.confirm('您要與他建立聊天嗎？') == true) {
                        var search_name = selected[0].split(',')[0];
                        var search_email = selected[0].split(',')[1];
                        var search_uid = selected[0].split(',')[2];
                        //在database裡面建立一個新的Chat節點
                        selectedEmail.push(user_email);
                        chatRef.push().set({
                            "Owner" : user_email+','+search_email,
                            "Start Time" : getTime(),
                            "Multi" : false,
                            "GroupName" : ',',
                        })
                        //更新雙方聊天對象chatObject
                        var object = firebase.database().ref('User/'+search_uid);
                        object.once('value').then(snapshot => {
                            var chatObject = snapshot.child('chatObject').val();
                            if (chatObject == ',' || chatObject == null) {object.update({"chatObject" : user_email});}
                            else {
                                var wasUpdate = false;
                                chatObject = chatObject.split(',');
                                for (var i = 0 ; i < chatObject.length ; i++) {
                                    if (chatObject[i] == user_email) {wasUpdate = true;break;}
                                }
                                if (!wasUpdate) {object.update({"chatObject" : chatObject +','+ user_email});}
                            }
                        }).catch(e => console.log(e.message));
        
                        var subject = firebase.database().ref('User/'+user_uid)
                        subject.once('value').then(snapshot => {
                            var chatObject = snapshot.child('chatObject').val();
                            if (chatObject == ',') {subject.update({"chatObject" : search_email});}
                            else {
                                var wasUpdate = false;
                                chatObject = chatObject.split(',');
                                for (var i = 0 ; i < chatObject.length ; i++) {
                                    if (chatObject[i] == search_email) {wasUpdate = true;break;}
                                }
                                if (!wasUpdate) {subject.update({"chatObject" : chatObject +','+ search_email});}
                            }
                        }).catch(e => console.log(e.message));
                    }
                }     
            }
            else {alert('已建立聊天')}
            selected = [];
            selectedEmail = [];
            document.getElementById('selected_user').innerHTML = "<button id='build_group' style='margin:5px 10px;border-radius:30px;font-size:16px;font-family:標楷體;' onclick=BuildChat();>Build Chat</button>";
            document.getElementById('selected_user').style.display = 'none';
        })
}

///取得聊天紀錄的節點，並更新ChatRoom
function GetMessage (type, id, clas) {
    talkee = clas.split(' ')[1];
    document.getElementById('TalkeeName').innerHTML = talkee;

    if (type == 'single') {
        users = id.split(',');
        chatRef.once('value').then(snapshot => {
            snapshot.forEach(childsnapshot => {
                ///取得聊天室節點的key
                var owners = childsnapshot.child('Owner').val().split(',');
                var foundusers = 0;
                for (var i = 0 ; i < owners.length ; i++) {
                    if (owners[i] == users[0]+'@gmail.com') {foundusers++;}
                    if (owners[i] == users[1]+'@gmail.com') {foundusers++;}
                    if (foundusers == owners.length) {
                        chatKey = childsnapshot.key;
                        ///更新ChatRoom
                        total_message = [];
                        document.getElementById("chat_room").innerHTML = "";
                        ///顯示聊天紀錄
                        ApperMessage();
                        break;
                    }
                }
            })
            ///切換div
            home_state = 1;
            document.getElementById('home_blk').style.display = 'none';
            document.getElementById('chat_blk').style.display = 'block';
            if (window.innerWidth < 700) {document.getElementById('object_blk').style.display= 'none'}
        }).catch(e => console.log(e.message));
    }
    else if (type == 'multi') {
        ///取得聊天室節點的key
        chatKey = id;
        ///更新ChatRoom
        total_message = [];
        document.getElementById("chat_room").innerHTML = "";
        ///顯示聊天紀錄
        ApperMessage();
        ///切換div
        home_state = 1;
        document.getElementById('home_blk').style.display = 'none';
        document.getElementById('chat_blk').style.display = 'block';
        if (window.innerWidth < 700) {document.getElementById('object_blk').style.display= 'none'}
    }
}

///顯示聊天紀錄
function ApperMessage () {
    var chatRoom = document.getElementById("chat_room");
    var chatAppear = firebase.database().ref('Chat/'+chatKey+'/message');
    var first_count = 0;
    var second_count = 0;

    chatAppear.once('value').then(snapshot => {
        snapshot.forEach(childsnapshot => {
            UserRef.once('value').then(Snapshot => {
                var icon;
                Snapshot.forEach(childSnapshot => {
                    if (childSnapshot.val().UserEmail == childsnapshot.val().deliver) {icon = childSnapshot.val().UserIcon;}
                })
                
                first_count += 1;
                if (childsnapshot.val().deliver == user_email) 
                    {total_message = pushMessage(childsnapshot.val(), 'deliver', icon, total_message);}
                else 
                    {total_message = pushMessage(childsnapshot.val(), 'receiver', icon, total_message);}
                
                chatRoom.innerHTML = total_message.join("");
                if (first_count != 0) {chatRoom.lastChild.scrollIntoView();} ///滾軸到最底部
            })
            
        });
        
        chatAppear.on('child_added', childsnapshot => {
            UserRef.once('value').then(Snapshot => {
                var icon;
                Snapshot.forEach(childSnapshot => {
                    if (childSnapshot.val().UserEmail == childsnapshot.val().deliver) {icon = childSnapshot.val().UserIcon;}
                })
                second_count += 1;
                if (second_count > first_count) {
                    if (childsnapshot.val().deliver == user_email) 
                        {total_message = pushMessage(childsnapshot.val(), 'deliver', icon, total_message);}
                    else 
                        {total_message = pushMessage(childsnapshot.val(), 'receiver', icon, total_message);}
                }
                chatRoom.innerHTML = total_message.join("");
                chatRoom.lastChild.scrollIntoView(); ///滾軸到最底部
            })
        });
    }).catch(e => console.log(e.message));
}

function pushMessage(value, type, icon, total_message) {
    if (value.type == 'text') {
        if (type == 'deliver')
            {total_message.push("<div class='deliver "+value.deliver.split('@')[0]+"'><span class='time'>"+value.time+"</span> <span class='Message text'>"+value.message+"</span><img src='img/Icon"+icon+".png' alt='icon' class='Icon deliver'></div>");}
        else 
            {total_message.push("<div class='receiver "+value.deliver.split('@')[0]+"'><img src='img/Icon"+icon+".png' alt='icon' class='Icon receiver'><span class='Message text'>"+value.message+"</span> <span class='time'>"+value.time+"</span></div>");}
    }
    else {
        var AppearH_W = ScalePicture(value.height, value.width); ///等比例縮放圖片
        if (type == 'deliver')
            {total_message.push("<div class='deliver "+value.deliver.split('@')[0]+"'><span class='time'>"+value.time+"</span> <img class='picture' src='"+value.message+"' style='height:"+AppearH_W[0]+";width:"+AppearH_W[1]+";'><img src='img/Icon"+icon+".png' alt='icon' class='Icon deliver'></div>");}
        else
            {total_message.push("<div class='receiver "+value.deliver.split('@')[0]+"'><img src='img/Icon"+icon+".png' alt='icon' class='Icon receiver'><img class='picture' src='"+value.message+"' style='height:"+AppearH_W[0]+";width:"+AppearH_W[1]+";'><span class='time'>"+value.time+"</span></div>");}
    }
    return total_message;
}

///顯示所有icon的div
function appearIcon() {
    var allicons = document.getElementById('allicons');
    if (allicons.style.display == 'none') {$('#allicons').slideDown();} ///jQuery的動畫
    else {$('#allicons').slideUp();} ///jQuery的動畫
}

///更新database和UserInfo的Icon，並關閉div
function changeIcon (num) {
    firebase.database().ref('User').child(user_uid).update({UserIcon:num})
    document.getElementById('currentIcon').src = 'img/InfoIcon' + num + '.png';
    $('#allicons').slideUp(); ///jQuery的動畫
}

function getTime () {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    if (month < 10) {month = '0' + month;}
    if (day < 10) {day = '0' + day;}
    if (hour < 10) {hout = '0' + hour;}
    if (min < 10) {min = '0' + min;}
    if (sec < 10) {sec = '0' + sec;}
    
    return year+'/'+month+'/'+day+' '+hour+':'+min+':'+sec;
};

async function notify (data, multi, groupname) {
    var objectIcon;
    ///取得傳遞者的Icon
    await UserRef.once('value').then(snapshot => {
        snapshot.forEach(childsnapshot => {
            if (childsnapshot.child('UserName').val() == data.deliver) {objectIcon = childsnapshot.child('UserIcon').val();}
        })
    }).catch(e => console.log(e.message));
    ///取得Icon後才送出通知 (需要await)
    if (Notification.permission == "granted") {
        if (data.type == 'text') {
            var notification;
            if (multi) {
                notification = new Notification(groupname, {
                    icon: 'img/icon'+'.ico',
                    body: data.deliver+'：'+data.message,
                });
            }
            else {
                notification = new Notification(data.deliver+'：', {
                    icon: 'img/icon'+'.ico',
                    body: data.message,
                });
            }
            notification.onclick = () => {notification.close();}
        }
        else {
            var notification;
            if (multi) {
                notification = new Notification(groupname, {
                    icon: 'img/icon'+'.ico',
                    body: data.deliver+'：',
                    image: data.message,
                });
            }
            else {
                notification = new Notification(data.deliver+'：', {
                    icon: 'img/Icon' + objectIcon + '.png',
                    image: data.message,
                });
            }
            notification.onclick = () => {notification.close();}
        }
    }
}

function initial_small_screen () {
    var userBlk = document.getElementById('user_blk');
    var mainBlk = document.getElementById('main_blk');
    var homeBlk = document.getElementById('home_blk');
    var chatBlk = document.getElementById('chat_blk');
    var objectBlk = document.getElementById('object_blk');
    var searchBlk = document.getElementById('search_blk');

    if (main_state == 0) {
        mainBlk.style.display = 'flex';
        userBlk.style.display = 'none';
    }
    else {
        mainBlk.style.display = 'none';
        userBlk.style.display = 'flex';
    }
    home_state = 0;
    homeBlk.style.display = 'none';
    chatBlk.style.display = 'none';
    obj_state = 0;
    objectBlk.style.display = 'flex';
    searchBlk.style.display = 'none';
}

function initial_big_screen () {
    var userBlk = document.getElementById('user_blk');
    var mainBlk = document.getElementById('main_blk');
    var homeBlk = document.getElementById('home_blk');
    var chatBlk = document.getElementById('chat_blk');
    var objectBlk = document.getElementById('object_blk');
    var searchBlk = document.getElementById('search_blk');
    if (main_state == 0) {
        mainBlk.style.display = 'flex';
        userBlk.style.display = 'none';
    }
    else {
        mainBlk.style.display = 'none';
        userBlk.style.display = 'flex';
    }
    if (home_state == 0) {
        homeBlk.style.display = 'block';
        chatBlk.style.display = 'none';
    }
    else {
        homeBlk.style.display = 'none';
        chatBlk.style.display = 'block';
    }
    if (obj_state == 0) {
        objectBlk.style.display = 'flex';
        searchBlk.style.display = 'none';
    }
    else {
        objectBlk.style.display = 'none';
        searchBlk.style.display = 'flex';
    }
}

///Change background color
function changeStyle (num) {
    var basicBlk = document.getElementById('basic_blk');
    var talkTo = document.getElementById('talkto');
    var sendBlk = document.getElementById('send_blk');
    if (num == 1) {
        basicBlk.style.backgroundColor = 'rgba(225, 255, 187,0.5)';
        talkTo.style.backgroundColor = 'rgb(137, 255, 137)';
        sendBlk.style.backgroundColor = 'rgb(137, 255, 137)';
    }
    else if (num == 2) {
        basicBlk.style.backgroundColor = 'rgba(211, 250, 250,0.5)';
        talkTo.style.backgroundColor = 'rgb(149, 229, 255)';
        sendBlk.style.backgroundColor = 'rgb(149, 229, 255)';
    }
    else {
        basicBlk.style.backgroundColor = 'rgba(253, 200, 209,0.5)';
        talkTo.style.backgroundColor = 'rgb(255, 177, 190)';
        sendBlk.style.backgroundColor = 'rgb(255, 177, 190)';
    }
}

///轉換特殊字元
function htmlspecialchars(ch) {
    if (ch===null) return '';
    ch = ch.replace(/&/g,"&amp;");
    ch = ch.replace(/\"/g,"&quot;");
    ch = ch.replace(/\'/g,"&#039;");
    ch = ch.replace(/</g,"&lt;");
    ch = ch.replace(/>/g,"&gt;");
    return ch;
}

///等比例縮放圖片
function ScalePicture(RealHeight, RealWidth) {
    var AppearHeight;
    var AppearWidth;
    var HtoWRatio = RealHeight/RealWidth; ///圖片高寬比
    var HeightRate = RealHeight/window.innerHeight*100/30;  ///圖片高度相對最大顯示高度
    var WidthRate = RealWidth/window.innerWidth*100/30;     ///圖片寬度相對最大顯示寬度

    if (HeightRate > 1 || WidthRate > 1) { ///高與寬皆在最大顯示範圍外
        ///相對高>=相對寬
        AppearHeight = 30;
        AppearWidth = AppearHeight/HtoWRatio;
        ///相對高<相對寬
        if (AppearWidth > 30) {
            AppearWidth = 30;
            AppearHeight = AppearWidth*HtoWRatio;
            return [AppearHeight+'vw',AppearWidth+'vw'];
        }
        else {return [AppearHeight+'vw',AppearWidth+'vw'];}
    }
    else { ///高與寬皆在最大顯示範圍內
        return [RealHeight+'px', RealWidth+'px'];
    }
}

window.onload = function () {
    init();
};