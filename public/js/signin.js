function initApp() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnResetPW = document.getElementById('btnResetPW');

    btnLogin.addEventListener('click', () => {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION).then(() => {
            firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
            .then( () => {       
                ///偵測用戶初次登入
                firebase.auth().onAuthStateChanged(user => {
                    if (user) {
                        firebase.database().ref('User').once('value')
                        .then(async snapshot => {
                            //尋找是否存在此用戶
                            var First = true;
                            snapshot.forEach(childsnapshot => {if (childsnapshot.key == user.uid) {First = false;}})
                            if (First) {
                                var name = prompt('請輸入暱稱',null);
                                if (name!=null && name!='' && name!='null') {
                                    firebase.database().ref('User/'+user.uid).set({
                                        "UserName" : name,
                                        "UserEmail" : user.email,
                                        "UserIcon" : 1,
                                        "chatObject" : ',',
                                        "chatGroup" : ',',
                                    })
                                    window.location.href='index.html';
                                }
                                else {
                                    await firebase.auth().signOut()
                                    .then( () => {alert('請重新登入並且輸入暱稱')})
                                    .catch(error => {alert(error.messege);})
                                    window.location.href='signin.html';
                                }
                            }
                            else {window.location.href='index.html';}
                        }).catch(e => console.log(e.message));
                    }
                })
            }).catch(error => {
                txtEmail.value = "";
                txtPassword.value = "";
                if (error.code == "auth/user-not-found") {create_alert("error", error.message);}
                else if (error.code == "auth/wrong-password") {create_alert("error", error.message);}
            });
        }).catch(e => console.log(e.message));
    });

    btnGoogle.addEventListener('click', () => {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION).then(() => {
            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).then(result => {
                ///偵測用戶初次登入
                firebase.auth().onAuthStateChanged(user => {
                    if (user) {
                        firebase.database().ref('User').once('value')
                        .then(async snapshot => {
                            //尋找是否存在此用戶
                            var First = true;
                            snapshot.forEach(childsnapshot => {if (childsnapshot.key == user.uid) {First = false;}})
                            if (First) {
                                var name = prompt('請輸入暱稱',null);
                                if (name!=null && name!='' && name!='null') {
                                    firebase.database().ref('User/'+user.uid).set({
                                        "UserName" : name,
                                        "UserEmail" : user.email,
                                        "UserIcon" : 1,
                                        "chatObject" : ',',
                                        "chatGroup" : ',',
                                    })
                                    window.location.href='index.html';
                                }
                                else {
                                    await firebase.auth().signOut()
                                    .then( () => {alert('請重新登入並且輸入暱稱')})
                                    .catch(error => {alert(error.messege);})
                                    window.location.href='signin.html';
                                }
                            }
                            else {window.location.href='index.html';}
                        }).catch(e => console.log(e.message));
                    }
                })
            }).catch(e => {
                console.log(e.code);
                console.log(e.message);
                console.log(e.email);
                console.log(e.credential);
            })
        }).catch(e => console.log(e.message));
    });

    btnSignUp.addEventListener('click', () => {
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(() => {
            txtEmail.value = "";
            txtPassword.value = "" ;
            create_alert("success", "Success creating account");
        }).catch((error) => {
            txtEmail.value = "";
            txtPassword.value = "" ;
            if (error.code == "auth/email-already-in-use") {create_alert("error", error.message);}
            else if (error.code == "auth/wrong-password") {create_alert("error", error.message);}
            else if (error.code == "auth/weak-password") {create_alert("error", error.message);}
        });
    });
    
    btnResetPW.addEventListener('click', () => {
        if (txtEmail.value == '') {alert('請輸入Email');}
        else {
            firebase.auth().sendPasswordResetEmail(txtEmail.value).then(() => {alert('已送出郵件');})
            .catch(e => {
                if (e.code == "auth/user-not-found") {alert("無此用戶")}
            })
        }
    })

    ///Create a new div with animation and Remove after end of animation
    btnLogin.addEventListener('mouseover', () => {
        var div = document.createElement('div');
        document.getElementById('body').appendChild(div);
        div.className = 'animate animate_1';
        div.addEventListener('animationend', () => {
            div.parentNode.removeChild(div);
        })
    })
    btngoogle.addEventListener('mouseover', () => {
        var div = document.createElement('div');
        document.getElementById('body').appendChild(div);
        div.className = 'animate animate_2';
        div.addEventListener('animationend', () => {
            div.parentNode.removeChild(div);
        })
    })
    btnSignUp.addEventListener('mouseover', () => {
        var div = document.createElement('div');
        document.getElementById('body').appendChild(div);
        div.className = 'animate animate_3';
        div.addEventListener('animationend', () => {
            div.parentNode.removeChild(div);
        })
    })
    btnResetPW.addEventListener('mouseover', () => {
        var div = document.createElement('div');
        document.getElementById('body').appendChild(div);
        div.className = 'animate animate_4';
        div.addEventListener('animationend', () => {
            div.parentNode.removeChild(div);
        })
    })
}
// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};