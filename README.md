# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : 聊天室
* Key functions
    1. 搜尋用戶
    2. 建立聊天
    3. 單人聊天
    4. 建立群組
    5. 多人聊天
    6. 載入聊天紀錄
    7. 聊天氣泡框
    
* Other functions
    1. 傳送圖片
    2. 用戶暱稱
    3. 更改背景顏色
    4. 用戶介面
    5. 更改用戶頭貼
    6. 更新聊天室的頭貼以及其他用戶聊天對象的頭貼
    7. 寄送密碼重設郵件 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midterm-d1b29.firebaseapp.com]

# Components Description : 
1. 搜尋用戶
    >登入之後，可以點擊<img src="RepoImg/search.png" width="13px"></img>來切換介面來顯示搜尋，點擊Search的按鈕後就可以顯示搜尋結果<sub>(附1)</sub>
    * 附1：<img src="RepoImg/search.jpg" width="200px"></img>
2. 建立聊天
    >若點擊搜尋到的用戶，搜尋介面就會變成這樣<sub>(附2)</sub>。點擊Build Chat的按鈕會跳出comfirm<sub>(附3)</sub>，確認後就可以與此用戶建立聊天。
    * 附2：<img src="RepoImg/search_S.jpg" width="200px"></img>
    * 附3：<img src="RepoImg/confirm_S.jpg" width="250px"></img>
3. 單人聊天
    >與用戶建立聊天之後，點擊<img src="RepoImg/home.png" width="13px"></img>將介面切換回顯示聊天對象<sub>(附4)</sub>。點擊聊天對象後可以顯示聊天室<sub>(附5)</sub>並進行聊天。
    * 附4：<img src="RepoImg/singlechat.jpg" width="200px"></img>
    * 附5：<img src="RepoImg/singlechatroom.jpg" width="300px"></img>
4. 建立群組
    >若在點擊Build Chat按鈕的時候，選取到的搜尋用戶超過兩個以上<sub>(附6)</sub>，就可以創建群組，並且跳出prompt<sub>(附7)</sub>，輸入群組名稱，輸入完成即可建立群組。
    * 附6：<img src="RepoImg/search_M.jpg" width="200px"></img>
    * 附7：<img src="RepoImg/prompt_M.jpg" width="250px"></img>
5. 多人聊天
    >建立群組之後，點擊<img src="RepoImg/home.png" width="13px"></img>將介面切換回顯示聊天對象<sub>(附8)</sub>。點擊聊天對象後可以顯示聊天室<sub>(附9)</sub>並進行聊天。
    * 附8：<img src="RepoImg/multichat.jpg" width="200px"></img>
    * 附9：<img src="RepoImg/multichatroom.jpg" width="300px"></img>
6. 載入聊天記錄
    >點擊聊天對象後，就可以載入聊天室，並且自動載入<strong>所有</strong>的聊天訊息。
7. 聊天氣泡框
    >用來區分是誰傳的訊息。若是user自己傳送的訊息，在聊天室內會靠右出現<sub>(附10)</sub>，而若不是user自己傳送的訊息，則會靠左出現<sub>(附11)</sub>。
    * 附10：<img src="RepoImg/deliver.jpg" width="200px"></img>
    * 附11：<img src="RepoImg/receiver.jpg" width="200px"></img>

# Other Functions Description(1~10%) : 
1. 傳送圖片
    >點擊<img src="RepoImg/upload.png" width="13px"></img>可以傳送圖片。若是圖片的高和寬都超過規定範圍，就會按照長寬比縮小至規定範圍。
2. 用戶暱稱
    >於初次登入時填入，在搜尋用戶的時候可以透過暱稱或是Email來搜尋。
3. 更改背景顏色
    >在整個頁面的右上角<sub>(附1)</sub>會顯示三種顏色，點擊後可以更改背景顏色。
    * 附1：<img src="RepoImg/backcolor.jpg" width="200px"></img>
4. 用戶介面
    >點擊<img src="RepoImg/userinfo.png" width="13px"></img>可以切換顯示用戶介面<sub>(附2)</sub>，在用戶介面中會顯示用度的相關訊息(Name、Email、UID)，
    * 附2：<img src="RepoImg/userinfo.jpg" width="300px"></img>
5. 更改用戶頭貼
    >在用戶介面中可以點擊ChangeIcon來更改自己的頭貼<sub>(附3)(附4)</sub>。
    * 附3：<img src="RepoImg/changeicon.jpg" width="300px"></img>
    * 附4：<img src="RepoImg/afterchange.jpg" width="300px"></img> 
6. 更新聊天室的頭貼以及其他用戶聊天對象的頭貼
    >當有用戶更改自己的頭貼時，每個用戶都會去搜尋更改頭貼的這個人是不是出現在自己的聊天對象，如果有就及時更新此人的頭貼，若是正在與此人聊天，則聊天室的頭貼也會即時更新。若是群組聊天，就算此人不是自己的聊天對象，聊天室的頭貼也會即時更新。
7. 寄送密碼重設郵件
    >在signin.html中有四個按鈕<sub>(附5)</sub>，若忘記自己密碼時，在輸入帳號後可以透過點擊第四個按鈕來發送重設密碼的郵件。此外，帳戶必須是已經經過申請，否則無法發送郵件。
    * 附5：<img src="RepoImg/resetpassword.jpg" width="200px"></img>

## Security Report :
1. database的Rule設定<sub>(附1)</sub>
    * 附1：<img src="RepoImg/databaseRule.jpg" width="200px"></img>
2. 進入聊天頁面時，會將觀察者狀態切換到當前用戶，若是用戶尚未登入，則不能使用頁面內的所有功能，除了SignIn跟切換背景顏色以外<sub>(附2)</sub>。此外，頁面中間會顯示出<sub>(附3)</sub>，點擊後會跳到Sigin頁面。
    * 附2：<img src="RepoImg/SignIn.jpg" width="200px"></img>
    * 附3：<img src="RepoImg/PleaseSignIn.jpg" width="200px"></img>
3. 當用戶確定登出之後，聊天頁面內的所有資料都會初始化，並且跳到SignIn頁面。
4. 若是用戶為初次登入，則會在database的User節點裡面建立屬於該用戶的資料<sub>(附3)</sub>。而當用戶登入之後，可以透過搜尋找到用戶，並建立聊天或是群組，並在database的Chat節點裡面建立該聊天或群組的資料，此外，在此聊天或群組的訊息都會儲存在此節點內<sub>(附4)</sub>。因此，只有該聊天或群組的Owner才能夠訪問該節點的所有資訊。
<br>至於User節點，則是所有登入後的用戶都可以訪問。之所以這麼做的原因是為了當搜尋用戶，或是顯示聊天對象，以及顯示聊天內容的時候，能夠取得用戶的Email來做比對，以及取得用戶的Icon來顯示在聊天室或是搜尋裡面。
<br>所以，在database裡面，有兩個主要的節點，一個是Chat，另一個是User。<sub>(附5)</sub>
    * 附3：<img src="RepoImg/user.jpg" width="200px"></img>
    * 附4：<img src="RepoImg/message.jpg" width="300px"></img>
    * 附5：<img src="RepoImg/reference.jpg" width="400px"></img>
5. 
6. 將&lt;Input type=&#039;text&#039;&gt;.value裡的符號轉換成特殊字元，可以避免&lt;iframe&gt;...的輸入。<sub>(附6)</sub>
    * 附6：<img src="RepoImg/security.jpg" width="200px"></img>